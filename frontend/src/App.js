import Login from './pages/Login';
import Workspaces from './pages/Workspaces';
import Main from './pages/Main';

import { BrowserRouter, Routes, Route } from "react-router-dom";
import { CustomerWrapper } from './services';

function App() {
  return (
    <CustomerWrapper>
      <BrowserRouter>
        <Routes>
          <Route path="/" exact element={<Login />} />
          <Route path="/login" element={<Login />} />
          <Route path="/workspaces" element={<Workspaces />} />
          <Route path="/main" element={<Main />} />
        </Routes>
      </BrowserRouter>
    </CustomerWrapper>
  );
}

export default App;
