export const LocalStorageService = {
    setClient: (client) => localStorage.setItem('client', JSON.stringify(client)),
    getClient: () => JSON.parse(localStorage.getItem('client'))
}