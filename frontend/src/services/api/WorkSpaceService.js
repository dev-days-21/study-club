import axios from "axios";
import { RoutesURL } from "../RoutesURL";
import { LocalStorageService } from '../';

export const WorkSpaceService = ({
    history = null,
    axiosInstance = axios.create({
        baseURL: RoutesURL.BASE_URL
    })
}) => {

    /**
     * 
     * @returns {Object}
     * ```json
     * [{
     *  "id": "string",
     *  "name": "string",
     *  "private": "boolean",
     *  "description": "string",
     *  "organization_id": "string"
     * }]
     * ```
     */
    const getAll = async () => {
        const res = await axiosInstance.post(RoutesURL.WORK_SPACE.GET_ALL, {
            id: LocalStorageService.getClient().id
        });
        return res.data;
    }

    return {
        getAll
    };
};