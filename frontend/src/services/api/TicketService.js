import axios from "axios";
import { RoutesURL } from "../RoutesURL";

export const TicketService = ({
    history = null,
    axiosInstance = axios.create({
        baseURL: RoutesURL.BASE_URL
    })
}) => {

    /**
     * @param   {Object} data
     * @param   {String} data.workspace_id
     * @returns {Array} 
     * ```json
     * [{
     *  "id": "string",
        "description": "string",
        "created_dttm": "string",
        "status": "string",
        "priority": 0,
        "workspace_id": "string",
        "responder_id": "string",
        "responder_message": "string",
        "expires_in": 0,
        "last_updated_dttm": "string"
     * }]
     * ```
    */
    const getAll = async (data) => {
        const res = await axiosInstance.post(RoutesURL.TICKET.GET_ALL, {
            "user_id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
            "tags": [
              {
                "id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
                "name": "text"
              }
            ],
            "text": "string"
          });
        return res.data;
    }

    const get = async (data) => {
        const { ticket_id, ...payload } = data;
        const res = await axiosInstance.post(RoutesURL.TICKET.GET(ticket_id), payload);
        console.log(res);
        return res.data;
    }

    const create = async (data) => {
        const res = await axiosInstance.post(RoutesURL.TICKET.CREATE, data);
        console.log(res);
        return res.data;
    }

    const subscribe = async (data) => {
        const res = await axiosInstance.post(RoutesURL.TICKET.SUBSCRIBE(data.ticket_id), data);
        return res.data;
    }

    const response = async (data) => {
        const {ticket_id, ...payload} = data;
        const res = await axiosInstance.post(RoutesURL.TICKET.RESPONSE(ticket_id), payload);
        return res.data;
    }

    const start = async (data) => {
        const { ticket_id, ...payload } = data;
        const res = await axiosInstance.post(RoutesURL.TICKET.START(data.ticket_id), payload);
        return res.data;
    }

    const finish = async (data) => {
        const { ticket_id, ...payload } = data;
        const res = await axiosInstance.post(RoutesURL.TICKET.FINISH(ticket_id), payload);
        return res.data;
    }

    const review = async (data) => {
        const { ticket_id, ...payload } = data;
        const res = await axiosInstance.post(RoutesURL.TICKET.REVIEW(ticket_id), payload);
        return res.data;
    }

    return {
        getAll,
        get,
        create,
        start,
        subscribe,
        response,
        finish,
        review
    };
};