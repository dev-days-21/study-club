import axios from "axios";
import { RoutesURL } from "../RoutesURL";

export const UserService = ({
    history = null,
    axiosInstance = axios.create({
        baseURL: RoutesURL.BASE_URL
    })
}) => {

    /**
     * 
     * @param {Object} data 
     * @param  {Array<String>} data.ids
     * @returns {Object}
     * ```json
     * [{
     *  "id": "",
     *  "last_name": "",
     *  "first_name": "",
     *  "balance": "number",
     *  "avatar": ""
     * }]
     * ```
     */
    const getAll = async (data) => {
        const res = await axiosInstance.post(RoutesURL.USER.GET_ALL, data);
        return res.data;
    }

    /**
     * 
     * @param {Object} data 
     * @param  {String} data.id
     * @returns {Object}
     * ```json
     * {
     *  "id": "",
     *  "last_name": "",
     *  "first_name": "",
     *  "balance": "number",
     *  "avatar": ""
     * }
     * ```
     */
    const get = async (data) => {
        const res = await axiosInstance.post(RoutesURL.USER.GET, data);
        return res.data;
    }

    const login = async (data) => {
        const res = await axiosInstance.post(RoutesURL.USER.LOGIN, data);
        return res.data;
    }

    return {
        getAll,
        login,
        get
    };
};