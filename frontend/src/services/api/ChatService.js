import axios from "axios";
import { RoutesURL } from "../RoutesURL";

export const ChatService = ({
    history = null,
    axiosInstance = axios.create({
        baseURL: RoutesURL.BASE_URL
    })
}) => {

    /**
     * @param   {Object} data
     * @param   {String} data.workspace_id
     * @returns {Array} 
     * ```json
     * [{
     *  "id": "string",
        "description": "string",
        "created_dttm": "string",
        "status": "string",
        "priority": 0,
        "workspace_id": "string",
        "responder_id": "string",
        "responder_message": "string",
        "expires_in": 0,
        "last_updated_dttm": "string"
     * }]
     * ```
    */
    const getAll = async (data) => {
        const res = await axiosInstance.post(RoutesURL.CHAT.GET_ALL(data.ticket_id), data);
        return res.data;
    }

    const send = async (data) => {
        const { ticket_id, ...payload } = data;
        const res = await axiosInstance.post(RoutesURL.CHAT.SEND(ticket_id), payload);
        return res?.data;
    }

    return {
        getAll,
        send
    };
};