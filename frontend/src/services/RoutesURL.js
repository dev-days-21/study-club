
export const RoutesURL = {
    BASE_URL: 'http://84.201.140.94/api',
    TICKET: {
        GET_ALL: `/ticket/all/${localStorage.getItem('wokspace_id') || ''}`,
        GET: (ticket_id = '') => `/ticket/get/${ticket_id}`,
        CREATE: `/ticket/create/${localStorage.getItem('wokspace_id') || ''}`,
        SUBSCRIBE: (ticket_id = '') => `/ticket/subscribe/${ticket_id}`,
        FINISH: (ticket_id = '') => `/ticket/finish/${ticket_id}`,
        RESPONSE: (ticket_id = '') => `/ticket/response/${ticket_id}`,
        START: (ticket_id = '') => `/ticket/start/${ticket_id}`,
        REVIEW: (ticket_id = '') => `/ticket/review/${ticket_id}`,
    },
    USER: {
        LOGIN: `/auth/login`,
        GET: `/user`,
        GET_ALL: `/workspace/users/${localStorage.getItem('wokspace_id') || ''}`, 
    },
    WORK_SPACE: {
        GET_ALL: `/workspace`
    },
    CHAT: {
        GET_ALL: (ticket_id = '') => `/chat/${ticket_id}`,
        SEND: (ticket_id = '') => `/chat/send/${ticket_id}`,
    }
}