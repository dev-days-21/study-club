import { TicketService as TS } from "./api/TicketService";
import { UserService as US } from "./api/UserService";
import { WorkSpaceService as WS } from "./api/WorkSpaceService";
import { ChatService as CS } from "./api/ChatService";

// import {  axiosInstancChatMock, axiosInstanceTicketMock, axiosInstanceWorkSpaceMock, axiosInstanceUserMock } from './mock/mock';

export const TicketService = TS({ /*axiosInstance: axiosInstanceTicketMock*/ });
export const UserService = US({ /*axiosInstance: axiosInstanceUserMock*/ });
export const WorkSpaceService = WS({ /*axiosInstance: axiosInstanceWorkSpaceMock */});
export const ChatService = CS({ /* axiosInstance : axiosInstancChatMock*/ });

export * from './localstorage/LocalStorageService';
export * from './auth/ClientWraper';
