import { RoutesURL } from "../RoutesURL";
import { v4 as uuidv4 } from 'uuid';

const users = [{
    "id": uuidv4(),
    "last_name": "",
    "first_name": "Elijah",
    "balance": 1000,
    "avatar": "E"
},
{
    "id": "not_me",
    "last_name": "",
    "first_name": "Fake",
    "balance": 100500,
    "avatar": "F"
}
]

const fake_tikets = [
    {
        id : uuidv4(),
        title : "Хочу разобраться с алгоритмами",
        description : "Не совсем понял последнюю тему, давайте соберемся и позанимаемся",
        status : "waiting",
        user_id : users[0].id,
        priority : 0,
        tags : ["Алгосы","co-working"]
    },
    {
        id : uuidv4(),
        title : "Написать бота для контест сервера",
        description : "Интересное задание от преподавателя: написать бота, для взаимодействия с контест сервером .",
        status : "waiting",
        user_id : users[0].id,
        priority : 0,
        tags : ["От преподавателя","БД"]
    },
    {
        id : uuidv4(),
        title : "Помогите установить ubuntu",
        description : "Кто может помочь установить? Решил попробовать linux",
        status : "waiting",
        user_id : users[0].id,
        priority : 0,
        tags : ["linux","help"]

        /*
        
        Кто хочет дополнительную лекцию по ФП
        Хочу лучше разлбраьтся в предмете, хочу послушать про монады

        
        */
    }
];

const fake_messages = [
    /*   */
];  

const work_spaces = [
    {
        "id": "7bd849bd-5b39-494b-b176-2bd813d2de01",
        "name": "ITMO JB",
        "private": true,
        "description": "38 участников",
        "organization_id": "148d264b-caf8-481f-aea7-a187a1251c00"
    },
    {
        "id": "b56707af-7186-4015-96c7-cbaa9a3b8dea",
        "name": 'Детский садик "Лучик',
        "private": false,
        "description": "322 участника",
        "organization_id": "25778a49-8b4a-471a-ba22-42eb76730df5"
    }
];


export const axiosInstanceTicketMock = {
    post: async (url, data) => {
        console.log('axiosInstanceTicketMock (data): ', data);
        console.log('axiosInstanceTicketMock (URL): ', url);

        switch (url) {
            case RoutesURL.TICKET.GET_ALL:
                return { data: fake_tikets };
            case RoutesURL.TICKET.CREATE:
                data.id = uuidv4();
                fake_tikets.push(data);
                return {

                }
            case RoutesURL.TICKET.RESPONSE:
                const obj1 = fake_tikets.find((s) => s.id === data.ticket_id);
                if(!obj1) return { };
                const idx1 = fake_tikets.indexOf(obj1);
                fake_tikets[idx1].status = "online";
                fake_tikets[idx1].start = Date.now();
                return {

                }
            case RoutesURL.TICKET.FINISH:
                const obj2 = fake_tikets.find((s) => s.id === data.ticket_id);
                if(!obj2) return { };
                const idx2 = fake_tikets.indexOf(obj2);
                fake_tikets[idx2].status = "finished";
                fake_tikets[idx2].finish = Date.now();
                fake_tikets[idx2].price = Math.floor((fake_tikets[idx2].finish - fake_tikets[idx2].start) / 1000);
                return {

                }
            case RoutesURL.TICKET.REVIEW:
                const obj3 = fake_tikets.find((s) => s.id === data.ticket_id);
                if(!obj3) return { };
                const idx3 = fake_tikets.indexOf(obj3);
                fake_tikets.splice(idx3,1);
                users[0].balance += obj3.price;
                return {
                    
                }

            case RoutesURL.TICKET.START:
                return {

                }
            default:
                break;
        }
    }
}


export const axiosInstanceWorkSpaceMock = {
    post: async (url, data) => {
        console.log('axiosInstanceWorkSpaceMock (data): ', data);
        console.log('axiosInstanceWorkSpaceMock (URL): ', url);

        switch (url) {
            case RoutesURL.WORK_SPACE.GET_ALL:
                return { data: work_spaces };
            default:
                break;
        }
    }
}

export const axiosInstanceUserMock = {
    post: async (url, data) => {
        console.log('axiosInstanceUserMock (data): ', data);
        console.log('axiosInstanceUserMock (URL): ', url);

        switch (url) {
            case RoutesURL.USER.GET:
                return { data: users[0] };
            case RoutesURL.USER.GET_ALL:
                return { data: users };
            default:
                break;
        }
    }
}

export const axiosInstancChatMock = {
    post: async (url, data) => {
        console.log('axiosInstancChatMock (data): ', data);
        console.log('axiosInstancChatMock (URL): ', url);

        const { ticket_id } = data;
        switch (url) {
            case RoutesURL.CHAT.GET_ALL:
                const ticket_msgs = fake_messages.filter(m => m.ticket_id === ticket_id);
                const answer = ticket_msgs.map(m => {
                return {
                        ...users.find(u => u.id === m.user_id),
                        ...m
                    };
                });
                return { data : answer };
            case RoutesURL.CHAT.SEND:
                data.id = uuidv4();
                fake_messages.push(data);
                return {

                }
        }
    }
};
