import { useState, createContext, useContext, useEffect } from "react";
import { LocalStorageService } from '../localstorage/LocalStorageService';


export const CustomerContext = createContext([]);
export const useCustomer = () => useContext(CustomerContext);

export const CustomerWrapper = ({ children }) => {
    const [client, setCustomer] = useState(null);
    const [working, setWorking] = useState(true);

    const uploadToken = () => {
        const client = LocalStorageService.getClient();

        if (client) {
            setCustomer(client);
        }
        
        setWorking(false);
    };

    useEffect(() => {
        uploadToken();
    }, []);

    return (
        <CustomerContext.Provider value={{ client, setCustomer }}>
            {working ? null : children}
        </CustomerContext.Provider>
    );
};