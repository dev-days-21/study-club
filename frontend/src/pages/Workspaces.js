import { useState, useEffect } from "react";
import { useCustomer, WorkSpaceService } from "../services";

function go(id) {
    return () => {
        localStorage.setItem('wokspace_id', id);
        window.location.href = "/main"
    }
}

function login() {
    window.location.href = "/"
}

function worksapce(ws, i) {
    return (
        <div key={i} className="p-2 ws-brick rounded" onClick={go(i)}>
            <div className="ws-avatar rounded-circle">{(ws.name[0] + "" + ws.name[1]).toUpperCase()}</div>
            <div className="ws-body">
                <div className="ws-name">{ws.name}</div>
                <div className="ws-desc text-muted text-desc">{ws.description}</div>
            </div>
        </div>
    )
}

export default function Workspaces() {
    const { client } = useCustomer();
    const [work_spaces, setWorkSpaces] = useState([]);

    useEffect(() => {
        const getData = () => {
            WorkSpaceService.getAll()
                .then((data) => {
                    setWorkSpaces(data);
                })
                .catch(console.error)
        };
        getData();
    }, []);

    if (!client || Object.keys(client).length === 0) {
        login();
        return;
    }

    return (
        <div className="page">
            <div className="container p-5">
                <h2>Workspaces</h2>
                <p className="text-muted text-desc">Выбери рабочее пространство, чтобы продолжить </p>
                <div className="mt-3">
                    {work_spaces.map((ws) => worksapce(ws, ws.id))}
                </div>
            </div>
        </div>
    )
}