import Board from "./Board";
import Ticket from "./Ticket";
import Create from "./Create";

import { TicketService, useCustomer } from '../services';
import { useState, useEffect } from 'react';

function login() {
    window.location.href = "/"
}

export default function Main()
{
    let k = 0;
    const [ counter, forceUpdate ] = useState(0);

    const [ compToRender, changeComponent ] = useState(0);
    const [ showadd, changeShowAdd ] = useState(true);
    const [ title, changeTitle ] = useState("");
    const [ data, changeData ] = useState({});

    const { client, setCustomer } = useCustomer();
    
    useEffect(() => {
        window.socket.on('UPDATE_SERVER', (event) => { 
            if(event.review)
            {
                setCustomer({...client, balance: client.balance - 5 });
            }
            else
            {
                console.log("I hear u!");
                forceUpdate(++k); 
            }
        });
        return function () {
            window.socket.removeAllListeners('UPDATE_SERVER');
        }
    }, []);

    if (!client || Object.keys(client).length === 0) {
        login();
        return;
    }

    const user = client;
    
    function back()
    {
        changeShowAdd(true);
        changeComponent(0)
        changeTitle("");
    }

    function open(data)
    {
        changeShowAdd(false);
        changeData(data);
        changeComponent(1);
        changeTitle(data.title);
    }

    function create()
    {
        changeShowAdd(false);
        changeComponent(2);
        changeTitle("Создание запроса");
        // forceUpdate(counter + 1);
    }

    function respond(id)
    {
        window.socket.emit("UPDATE_CLIENT",{});
        TicketService.response({ ticket_id : id, id: user.id });
        back();
    }

    function start(ticket)
    {
        // window.socket.emit("UPDATE_CLIENT",{});
        TicketService.start({ ticket_id : ticket.id, id : user.id });
    }

    function finish(ticket)
    {
        window.socket.emit("UPDATE_CLIENT",{});
        TicketService.finish({ ticket_id : ticket.id, id : user.id })
            .catch(console.error);
        back();
    }

    function review(id)
    {
        window.socket.emit("UPDATE_CLIENT",{ review : true, id : id });
        //TicketService.review({ ticket_id : id, id : user.id });
        setCustomer({...client, balance: 110 });
        back();
    }

    function AddBtn()
    {
        return (
            <div onClick={create} className="ticket-add-btn rounded-circle">
                <div className="line"></div>
                <div style={{transform:"rotate(90deg)"}} className="line"></div>
            </div>
        );
    }

    function StatusBar()
    {
        return (
            <header className="main-header status-bar-header d-flex">
                <button style={{marginLeft:-10}} onClick={back} className="btn btn-dark btn-sm">Назад</button>
                <div className="header-title">{title}</div>
            </header>
        )
    }

    function Header()
    {
        return (
            <header className="main-header just-cont status-bar-header d-flex">
                <div className="u-avatar rounded">{user.first_name[0]}</div>
                <div className="money rounded">{user ? user.balance : ""} m'</div>
            </header>
        )
    }

    return (
        <div className="page" data-val={counter}>
            <div className="main-board container-lg">
                { !showadd ? <StatusBar /> : <Header /> }
                <div className="main-component-wrapper">
                    {
                        compToRender === 0 ?
                        <Board open={open} counter={counter} />
                        : compToRender === 1 ?
                        <Ticket counter={counter} start={start} user={user} review={review} finish={finish} respond={respond} back={back} data={data} />
                        : compToRender === 2 ?
                        <Create counter={counter} back={back} />
                        : ""
                    }
                    { showadd ? <AddBtn /> : "" }
                </div>
                <div style={{top:-100,position:'absolute'}}>{counter}</div>
            </div>
        </div>
    );
}
