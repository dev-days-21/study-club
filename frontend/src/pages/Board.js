import { useState, useEffect } from "react";
import { TicketService } from '../services'
import Tag from "./partials/Tags";

export default function Board(props) {
    const { open, counter } = props;
    const [tikets, setTickets] = useState([]);

    useEffect(() => {
        console.log("BOARD ONLINE");
        const getData = () => {
            TicketService.getAll()
                .then((data) => {
                    console.log(data);
                    setTickets(data);
                })
                .catch(console.error)
        };
        getData();
        return () => { console.log("BOARD DIED"); }
    }, [counter]);

    const finished = tikets.Waiting_for_review || []; //.filter(t => t.status === 'finished');
    const online =  tikets.In_progress || [];//.filter(t => t.status === 'online');
    const active =  tikets.Open || [];//.filter(t => t.status === 'waiting');

    function regular_ticket(tk, i) {
        const subs = tk.subs > 1 ? <span className="tk-badge">{"+" + tk.subs}</span> : "";

        return (
            <div onClick={() => { open(tk); }} key={i} className="p-3 tk-brick rounded">
                <div className="tk-name">{tk.title}{subs}</div>
                <div className="tk-tags">{tk.tags.map((t, j) => { return (<Tag name={t} key={`ticket_${i}_tag${j}`} />) })}</div>
                <div className="tk-desc text-muted text-desc">{tk.description}</div>
            </div>
        )
    }

    function finished_ticket(tk, i) {
        return (
            <div onClick={() => { open(tk); }} key={i} className="tk-brick tk-brick-finished rounded">
                <div className="tk-name">{tk.title}</div>
            </div>
        )
    }

    function online_ticket(tk, i) {
        return (
            <div onClick={() => { open(tk); }} key={i} className="tk-brick tk-brick-online rounded">
                <div className="tk-name">{tk.title}</div>
            </div>
        )
    }

    return (
        <div className="board container">
            {
                finished.length ?
                    (<div>
                        <div className="bd-name p-2">Ожидает ревью</div>
                        <div className="board-finished d-flex flex-wrap">
                            {finished.map(finished_ticket)}
                        </div>
                    </div>)
                    : ""
            }
            {
                online.length ?
                    (<div>
                        <div className="bd-name p-2">Онлайн</div>
                        <div className="board-online d-flex flex-wrap">
                            {online.map(online_ticket)}
                        </div>
                    </div>)
                    : ""
            }
            <div className="bd-name p-2 text-mute">Доступные</div>
            <div className="board-active d-flex flex-wrap">
                {active.map(regular_ticket)}
            </div>
        </div>
    );
}