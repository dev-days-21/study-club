import randomColor from 'randomcolor';

export default function Tag(props)
{
    let { name, action } = props;
    console.log(name);
    const color = randomColor({ luminosity : "dark", seed : name.name });

    if(!action || typeof action !== 'function') action = () => { };

    return (
        <span onClick={action} style={{backgroundColor : color }} className="tk-tag rounded">#{name.name}</span>
    )
}
