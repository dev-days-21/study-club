
import { useCustomer } from '../services';

export default function Review(props)
{
    const { data, review } = props;
    const { client } = useCustomer();

    function Helper()
    {
        return (
            <div className="review container p-3">
                <div className="reward container">
                    <div>
                        <div className="money-big">+{data.requested_reward }</div>
                        <div className="money-desc">Баланс</div>
                    </div>
                </div>
                <h2>Как прошло?</h2>
                <div className="review-form">
                    <div className="review-mark">
                        <label htmlFor="help_range_1" className="form-label">Понравилось ли Вам общение?</label>
                        <input type="range" className="form-range" min="0" max="5" step="0.5" id="help_range_1"/>
                    </div>
                    <textarea placeholder="Комментарий" className="form-control mt-2" rows="5" />
                    <button style={{marginTop:30}} onClick={() => review(data.id)} className="btn btn-primary">Отправить</button>
                </div>
            </div>
        );
    }

    function Subscriber()
    {
        return (
            <div className="review container p-3">
                <h2>Как прошло?</h2>
                <div className="review-mark">
                    <label htmlFor="sub_range_1" className="form-label">Ты решил изначальную задачу?</label>
                    <input type="range" className="form-range" min="0" max="5" step="0.5" id="sub_range_1" />
                </div>
                <div className="review-mark">
                    <label htmlFor="sub_range_2" className="form-label">Было полезно?</label>
                    <input type="range" className="form-range" min="0" max="5" step="0.5" id="sub_range_2" />
                </div>
                <div className="review-mark">
                    <label htmlFor="sub_range_3" className="form-label">Ты хорошо провел время?</label>
                    <input type="range" className="form-range" min="0" max="5" step="0.5" id="sub_range_3" />
                </div>
                <textarea placeholder="Комментарий" className="form-control mt-2" rows="5" />
                <button onClick={() => review(data.id)} className="btn btn-primary">Отправить</button>
            </div>
        );
    }

    return (
        <div>
            {
                data.responder_id === client.id ?
                <Helper /> :
                <Subscriber />
            }
        </div>
    );
}
