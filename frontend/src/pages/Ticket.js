import { useEffect, useState } from "react";
import { ChatService } from "../services";
import Tag from "./partials/Tags";
import Review from "./Review";

export default function Ticket(props) {

    const { data, respond, finish, start, review, user, counter } = props;
    const { status } = data;

    let component = (<div></div>);
    
    if (status === 'Open') component = <ActiveTicket />;
    if (status === 'Chatting') component = <Chat />;
    if (status === 'In_progress') component = <Chat />;
    if (status === 'Waiting_for_review') component = <Review data={data} review={review} />;

    function ActiveTicket() {
        return (
            <div className="ticket-active">
                <div className="ticket-active-meta">
                    <h2 style={{ marginBottom: 0 }}>{data.title}</h2>
                    <div style={{ marginBottom: 8 }} className="tk-tags">{data.tags.map((t, j) => { return (<Tag name={t} key={`ticket_tag${j}`} />) })}</div>
                    <p>{data.description}</p>
                </div>
                <div className="ticket-active-desc">
                    <p className="text-muted">
                        Мы все еще ждем человека, который готов помочь с этой задачей.
                        Им можешь стать ты! Или можешь подписаться на карточку.
                    </p>
                </div>
                {
                !(data.requesters_ids && data.requesters_ids.includes(user.id)) ?
                <button onClick={() => respond(data.id)} className="btn btn-ticket btn-primary">Могу помочь</button>
                : ""}
            </div>
        );
    }

    function Chat() {
        const [history, updateHistory] = useState([]);
        let [interval, setI] = useState(0);

        function message(msg, idx) {
            const me = msg.me ? "msg-me" : "msg-not-me";
            return (
                <div key={idx} className="chat-msg">
                    <div style={{ marginLeft: 0, marginRight: 0, paddingLeft: 40, paddingRight: 40 }} className={`row ${me}`}>
                        <div style={{}} className={`msg-avatar rounded-circle`}>{msg.user.id[0].toUpperCase()}</div>
                        <div style={{ width: 'auto', maxWidth: '80%' }} className={`chat-body rounded ${me}`}>
                            <div className="msg-text">{msg.text}</div>
                        </div>
                    </div>
                    <div style={{display:'none'}}>{counter}</div>
                </div>
            );
        }

        useEffect(() => {
            update();
        }, []);

        function update() {
            ChatService.getAll({ ticket_id: data.id, id: user.id })
                .then((res) => {
                    console.log(res);
                    const answer = res.map(m => {
                        if (m.user.id === user.id)
                            return { ...m, me: true };
                        return { ...m, me: false };
                    });
                    updateHistory(answer.reverse());
                })
                .catch(console.error)
        }

        function send() {
            const text = document.getElementById('msgInput').value;

            document.getElementById('msgInput').value = "";
            ChatService.send({
                ticket_id: data.id,
                user_id: user.id,
                text: text
            }).then(
                () => {
                    window.socket.emit('UPDATE_CLIENT', { a: 1 });
                }
            )
                .catch(console.error);
        }

        let progress = 0;
        const max_progress = 3600;
        function start_progress() {
            if (interval) {
                finish(data);
                clearInterval(interval);
                setI(0);
                return;
            }

            // Понимаю, что можно через state, но так вроде проще, хз.
            const bar = document.getElementById('ticketProgressBar');
            const btn = document.getElementById('ticketStartButton');
            const cnt = document.getElementById('timeCounter');
            const bnr = document.getElementById('startTimeBanner');

            btn.innerText = "Завершить";

            const now = new Date();
            const ms = now.getMinutes();
            const hs = now.getHours();

            bnr.innerHTML = `${("" + hs).padStart(2, '0')}:${("" + ms).padStart(2, '0')}`;

            document.getElementById('startStat').style.visibility = 'visible';

            let prev = Date.now();
            interval = setInterval(() => {

                if (progress > max_progress) clearInterval(interval);

                progress += (Date.now() - prev) / 1000;
                prev = Date.now();

                bar.style.width = `${progress / max_progress * 100}%`;

                const time = Math.floor(progress);
                const seconds = time % 60;
                const minutes = Math.floor(time / 60) % 60;
                const hours = Math.floor(Math.floor(time / 60) / 60) % 60;

                const text = `${("" + hours).padStart(2, '0')}:${("" + minutes).padStart(2, '0')}:${("" + seconds).padStart(2, '0')}`
                cnt.innerHTML = text;

            }, 100);

            start(data);
            setI(interval);
        }

        return (
            <div className="ticket-chat">
                {data.responder_id === user.id ?
                    (<div>
                        <div className="ticket-control container">
                            <button id="ticketStartButton" onClick={start_progress} style={{ marginRight: 10 }} className="btn btn-primary">Начать!</button>
                            <div className="progress" style={{ width: "100%", height: 38 }}>
                                <div id="ticketProgressBar" className="progress-bar progress-bar-animated progress-bar-striped" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div id="startStat" style={{ visibility: 'hidden' }} className="container d-flex">
                            <div className="ticket-time-started">
                                <div id="startTimeBanner" className="tts-down">22:30</div>
                            </div>
                            <div className="ticket-time-added">+<span id="timeCounter" className="time-added">00:00:31</span></div>
                        </div>
                    </div>)
                    : ""}
                <div className="chat-list">
                    <div className="chat-list-wrapper container">
                        {history.map(message)}
                    </div>
                </div>
                <div className="buttom-msg-input">
                    <div className="container">
                        <div className="input-group ">
                            <input onKeyDown={e => e.code === "Enter" ? send() : ""} id="msgInput" className="form-control" type='text' />
                            <button onClick={send} className="btn btn-outline-primary" >Send</button>
                        </div>
                    </div>
                </div>
                <div style={{display:'none'}}>{counter}</div>
            </div>
        );
    }

    return (
        <div className="ticket container">
            {component}
        </div>
    )

}
