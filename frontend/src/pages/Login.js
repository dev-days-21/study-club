import { UserService, LocalStorageService, useCustomer } from "../services";

function go() {
    window.location.href = "/workspaces"
}

export default function Login() {
    const { client, setCustomer } = useCustomer();

    const onSubmit = () => {
        const [email, password] = document.querySelectorAll('input[type]');

        console.log(email.value);
        UserService
            .login({
                "email": email.value,
                "password": password.value
            })
            .then((client) => {
                console.log(client);
                LocalStorageService.setClient(client);
                setCustomer(client);
                // go();
            })
            .catch(console.error)
    };

    if (client) {
        go();
        return;
    }

    return (
        <div className="page">
            <div className="container h-100 d-flex align-items-center justify-content-center">
                <div className="p-3">
                    <div className="logo">
                        SC
                        <span>SC</span>
                    </div>
                    <div className="input-form mt-3">
                        <input onKeyDown={e => e.code === "Enter" ? onSubmit() : ""} className="form-control mt-3" name="sc-login" placeholder="Username" type="email" />
                        <input onKeyDown={e => e.code === "Enter" ? onSubmit() : ""} className="form-control mt-2" name="sc-login" placeholder="Password" type="password" />
                        <button onClick={onSubmit} className="btn btn-primary mt-3 w-100">Log in</button>
                        <button onClick={go} className="btn btn-light mt-2 w-100">Register</button>
                        <p className="test-users" style={{  marginTop: 30 }}>
                            username : alisa, password : 1234
                        </p>
                        <p className="test-users">
                            username : alexa, password : 1234
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}
