import { useState } from "react";
import Tag from "./partials/Tags";
import { TicketService, useCustomer } from '../services';

export default function Create(props) {
    const [tags, updateTags] = useState([]);
    const { client } = useCustomer();
    const { back } = props;


    function tag(t, j) {
        const me = j;

        function remove() {
            tags.splice(me, 1);
            updateTags([...tags]);
        }

        return (
            <Tag action={remove} name={t} key={`ticket_t${j}`} />
        )
    }

    function add_tag() {
        const tag = document.getElementById('tagInput').value;
        tags.push({ name : tag });
        updateTags([...tags]);
        document.getElementById('tagInput').value = "";
    }

    function create() {
        // Можно прям react-style, но так быстрее
        const name = document.getElementById('ticketName').value;
        const desc = document.getElementById('ticketDesc').value;

        TicketService.create({
            title : name,
            user_id: client.id,
            description: desc,
            // status : "waiting",
            // priority : 0,
            tags : tags
        });

        window.socket.emit("UPDATE_CLIENT",{});

        back();
    }

    return (
        <div className="container ticket-create p-3">
            <div className="ticket-form">
                <div className="tk-tags">{tags.map(tag)}</div>
                <div className="ticket-form-header-wrapper">
                    <input id="ticketName" className="form-control" aria-label="Large" placeholder="Запрос" type="text" />
                </div>
                <textarea id="ticketDesc" placeholder="Описание" className="form-control mt-2" rows="5" />
            </div>
            <div className="input-group mt-2">
                <input onKeyDown={e => e.code === "Enter" ? add_tag() : ""} id="tagInput" placeholder="Тэг" className="form-control" type='text' />
                <button onClick={add_tag} className="btn btn-outline-primary" >+</button>
            </div>
            
            <button onClick={create} className="btn btn-ticket btn-primary">Создать</button>
        </div>
    );
}
