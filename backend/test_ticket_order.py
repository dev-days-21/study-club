tickets_by_status = {"Open": [], "In_progress": [], "Chatting": [], "Waiting_for_review": []}

for status in tickets_by_status.keys():
    print(status)
    subq = Ticket.query \
        .join(TicketRequester, Ticket.id==TicketRequester.ticket_id) \
        .add_columns(
            Ticket.id,
            func.count(TicketRequester.requester_user_id).label("user_count")) \
        .filter(Ticket.workspace_id == workspace_id) \
        .filter(Ticket.status == status) \
        .group_by(Ticket.id) \
        .all()

    q = Ticket.query \
        .join(subq, Ticket.id==subq.id) \
        .add_columns(
            Ticket.id) \
        .order_by(func.desc(subq.user_count)) \
        .all()

    tickets_by_status.append(q)

    break