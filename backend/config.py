# from sqlalchemy import create_engine
# from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy.orm import sessionmaker

import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    # SECRET_KEY = 'this-really-needs-to-be-changed'    
    SQLALCHEMY_DATABASE_USER = os.environ['POSTGRES_USER']
    SQLALCHEMY_DATABASE_PW = os.environ['POSTGRES_PW']
    SQLALCHEMY_DATABASE_DB = os.environ['POSTGRES_DB']
    SQLALCHEMY_DATABASE_URI = "postgresql://" + SQLALCHEMY_DATABASE_USER + ":" + SQLALCHEMY_DATABASE_PW + "@localhost/" + SQLALCHEMY_DATABASE_DB
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_ECHO = True
