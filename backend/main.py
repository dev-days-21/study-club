from itertools import groupby
import os
import sys
from datetime import date, datetime
import json
from typing import Text

from flask import Flask, request, redirect, url_for
from flask.helpers import total_seconds
from flask_restx import Resource, Api, fields
from werkzeug.middleware.proxy_fix import ProxyFix
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, func, cast, Text
from werkzeug.wrappers import response
from werkzeug.security import generate_password_hash, check_password_hash
import logging
import psycopg2

app = Flask(__name__)

env_config = os.getenv("APP_SETTINGS", "config.DevelopmentConfig")
app.config.from_object(env_config)

# logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
# rootLogger = logging.getLogger()

# fileHandler = logging.FileHandler("{0}/{1}.log".format("/home/gitlab-runner/builds/xmVTVh3Z/0/dev-days-21/study-club/backend", "test"))
# fileHandler.setFormatter(logFormatter)
# rootLogger.addHandler(fileHandler)

# consoleHandler = logging.StreamHandler()
# consoleHandler.setFormatter(logFormatter)
# rootLogger.addHandler(consoleHandler)

db = SQLAlchemy(app)

class Ticket(db.Model):
    __tablename__ = 'ticket'
    __table_args__ = {
        'autoload': True,
        'schema': 'study_center',
        'autoload_with': db.engine
    }

class User(db.Model):
    __tablename__ = 'user'
    __table_args__ = {
        'autoload': True,
        'schema': 'study_center',
        'autoload_with': db.engine
    }

class Workspace(db.Model):
    __tablename__ = 'workspace'
    __table_args__ = {
        'autoload': True,
        'schema': 'study_center',
        'autoload_with': db.engine
    }

class TicketRequester(db.Model):
    __tablename__ = 'ticket_requester'
    __table_args__ = {
        'autoload': True,
        'schema': 'study_center',
        'autoload_with': db.engine
    }

class WorkspaceUser(db.Model):
    __tablename__ = 'workspace_user'
    __table_args__ = {
        'autoload': True,
        'schema': 'study_center',
        'autoload_with': db.engine
    }

class Message(db.Model):
    __tablename__ = 'message'
    __table_args__ = {
        'autoload': True,
        'schema': 'study_center',
        'autoload_with': db.engine
    }

class TicketReview(db.Model):
    __tablename__ = 'ticket_review'
    __table_args__ = {
        'autoload': True,
        'schema': 'study_center',
        'autoload_with': db.engine
    }

class TicketTag(db.Model):
    __tablename__ = 'ticket_tag'
    __table_args__ = {
        'autoload': True,
        'schema': 'study_center',
        'autoload_with': db.engine
    }



app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, title="Study Center API", doc="/api/", prefix="/api/", default ='API methods', default_label='')
ns = api.namespace("api")

app.config["RESTX_MASK_HEADER"] = "payload"

default_field = {"uuid": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
                 "dttm": "2004-10-19 10:23:54",
                 "dt": "2004-10-19",
                 "email": "example@example.com",
                 "str": "text",
                 "int": 0,
                 "bool": False}

user_auth_model = api.model("Auth", {
    "email": fields.String(default=default_field["email"], required=True),
    "password": fields.String()
})

register_model = api.model("Register", {
    "email":      fields.String(default=default_field["email"], required=True),
    "last_name":  fields.String(default=default_field["str"], required=True),
    "first_name": fields.String(default=default_field["str"], required=True),
    "birth_date": fields.String(default=default_field["dt"], required=False),
    "password":   fields.String(default=default_field["str"], required=True),
    "avatar":     fields.String(),
})

user_request_model = api.model("GetUser", {
    "id":         fields.String(default=default_field["uuid"], required=True)
})

user_response_model = api.model("User", {
    "id":         fields.String(default=default_field["uuid"], required=True),
    "email":      fields.String(default=default_field["email"], required=True),
    "last_name":  fields.String(default=default_field["str"], required=True),
    "first_name": fields.String(default=default_field["str"], required=True),
    "birth_date": fields.String(default=default_field["dt"], required=False),
    "balance":    fields.Integer(default=default_field["int"], required=True),
    "avatar":     fields.String(),
})

tag_model = api.model("Tag", {
    "name": fields.String(default=default_field["str"], required=True)
})

ticket_model = api.model("Ticket", {
    "id":                fields.String(default=default_field["uuid"], required=True),
    "title":             fields.String(default=default_field["str"], required=True),
    "description":       fields.String(default=default_field["str"], required=True),
    "created_dttm":      fields.String(default=default_field["dttm"], required=True),
    "status":            fields.String(default=default_field["str"], required=True),
    "priority":          fields.Integer(default=default_field["int"], required=True),
    "workspace_id":      fields.String(default=default_field["uuid"], required=True),
    "responder_id":      fields.String(),
    "responder_message": fields.String(),
    "expires_in":        fields.Integer(default=default_field["int"], required=True),
    "last_updated_dttm": fields.String(default=default_field["dttm"], required=True),
    "requesters_ids":    fields.List(fields.String(default=default_field["uuid"], required=True)),
    "tags":              fields.List(fields.Nested(tag_model), allow_null=False),
    "started_dttm":      fields.Integer(),
    "finished_dttm":     fields.Integer(),
    "requested_reward":  fields.Integer()
})

create_ticket_model = api.model("CreateTicket", {
    "user_id":           fields.String(default=default_field["uuid"], required=True),
    "title":             fields.String(default=default_field["str"], required=True),
    "description":       fields.String(default=default_field["str"], required=True),
    "tags":              fields.List(fields.Nested(tag_model), allow_null=False)
})

reward_model = api.model("Reward", {
    "user_id":           fields.String(default=default_field["uuid"], required=True),
    "reward":           fields.String(default=default_field["int"], required=True)
})

search_model = api.model("Search", {
    "user_id": fields.String(default=default_field["uuid"], required=True),
    "tags":    fields.List(fields.Nested(tag_model), allow_null=False),
    "text":    fields.String()
})

tickets_model = api.model("TicketsList", {
    "Open":     fields.List(fields.Nested(ticket_model, allow_null=False)),
    "In_progress":     fields.List(fields.Nested(ticket_model, allow_null=False)),
    "Waiting_for_review":     fields.List(fields.Nested(ticket_model, allow_null=False))
    # "Expired": fields.List(fields.Nested(ticket_model, allow_null=False)),
    # "Closed":      fields.List(fields.Nested(ticket_model, allow_null=False))
})

@api.response(403, "Not allowed")
@api.route("/ticket/get/<string:ticket_id>")
class GetTicket(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_with(ticket_model, mask=None)
    def post(self, ticket_id):
        t = Ticket.query.filter_by(id=ticket_id).first()
        response = {(k):(v.strftime("%Y-%m-%d %H:%M:%S") if k in ["created_dttm", "last_updated_dttm"] else
                        int((v - datetime(1970, 1, 1)).total_seconds())if v is not None else None if k in ["started_dttm", "finished_dttm"] else 
                        v) for k, v in t.__dict__.items() if k !=   "_sa_instance_state" and v is not None}
        print(response)

        ticket_tags = TicketTag.query \
            .with_entities(
                func.string_agg(TicketTag.tag, ',').label("requesters_id")) \
            .filter_by(ticket_id=ticket_id) \
            .group_by(TicketTag.ticket_id) \
            .all()

        requesters = TicketRequester.query \
            .filter_by(ticket_id=ticket_id) \
            .all()

        print(requesters)

        if ticket_tags:
            response["tags"] = [{"name":tag} for tag in ticket_tags[0][0].split(',')]
        else:
            response["tags"] = []

        response["requesters_ids"] = [requester.requester_user_id for requester in requesters]
            
        return response

        

@api.response(403, "Not allowed")
@api.route("/ticket/all/<string:workspace_id>")
class WorkspaceTickets(Resource):
    @api.expect(search_model, validate=True)
    @api.marshal_with(tickets_model, mask=None)
    def post(self, workspace_id):
        print(workspace_id)
        tickets_by_status = {"Open": [], "In_progress": [], "Chatting": [], "Waiting_for_review": []}

        for status in tickets_by_status.keys():
            ticket_requesters = Ticket.query \
                .with_entities(
                    Ticket.id,
                    func.count(TicketRequester.requester_user_id).label("user_count"),
                    func.string_agg(TicketRequester.requester_user_id.cast(Text), ',').label("requesters_id")) \
                .join(TicketRequester, Ticket.id==TicketRequester.ticket_id) \
                .filter(Ticket.workspace_id == workspace_id) \
                .filter(Ticket.status == status) \
                .group_by(Ticket.id) \
                .subquery()

            ticket_tags = Ticket.query \
                .with_entities(
                    Ticket.id,
                    func.string_agg(TicketTag.tag, ',').label("tags")) \
                .join(TicketTag, TicketTag.ticket_id==Ticket.id) \
                .filter(Ticket.workspace_id == workspace_id) \
                .filter(Ticket.status == status) \
                .group_by(Ticket.id) \
                .subquery()

            tickets = Ticket.query \
                .join(ticket_requesters, Ticket.id==ticket_requesters.c.id) \
                .join(ticket_tags, Ticket.id==ticket_tags.c.id, isouter=True) \
                .add_columns(
                    ticket_requesters.c.requesters_id,
                    ticket_tags.c.tags) \
                .order_by(ticket_requesters.c.user_count.desc()) \
                .all()    

            for ticket in tickets:
                print(ticket)
                tags = ticket[2]
                if tags:
                    tag_dict_list = [{"name":tag} for tag in tags.split(',')]
                else:
                    tag_dict_list = []

                tickets_by_status[status].append({
                    "id": ticket[0].id,
                    "title": ticket[0].title,
                    "description": ticket[0].description,
                    "created_dttm": ticket[0].created_dttm.strftime("%Y-%m-%d %H:%M:%S"),
                    "status": ticket[0].status,
                    "priority": ticket[0].priority,
                    "workspace_id": ticket[0].workspace_id,
                    "responder_id": ticket[0].responder_id,
                    "responder_message": ticket[0].responder_message,
                    "expires_in": ticket[0].expires_in,
                    "last_updated_dttm": ticket[0].last_updated_dttm.strftime("%Y-%m-%d %H:%M:%S"),
                    "requesters_id": ticket[1].split(','),
                    "started_dttm": int((ticket[0].started_dttm - datetime(1970, 1, 1)).total_seconds()) if ticket[0].started_dttm is not None else None,
                    "finished_dttm": int((ticket[0].finished_dttm - datetime(1970, 1, 1)).total_seconds()) if ticket[0].finished_dttm is not None else None,
                    "requested_reward": int((ticket[0].finished_dttm - ticket[0].started_dttm).total_seconds()) if ticket[0].started_dttm is not None and ticket[0].finished_dttm is not None else None,
                    "tags": tag_dict_list
                })

        print(tickets_by_status)
        tickets_by_status["In_progress"] += tickets_by_status["Chatting"]
        return tickets_by_status

@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/ticket/create/<string:workspace_id>')
class CreateTicket(Resource):
    @api.expect(create_ticket_model, validate=True)
    @api.marshal_with({}, mask=None)
    def post(self, workspace_id):
        data = json.loads(request.data)
        print(data)
        tags = data["tags"]
        

        ticket = Ticket(
            title=data["title"],
            description=data["description"],
            workspace_id = workspace_id,
            status="Open"
            )
        db.session.add(ticket)    
        db.session.commit()
        db.session.refresh(ticket)

        ticket_request = TicketRequester(requester_user_id=data["user_id"], ticket_id=ticket.id)
        print(ticket_request)
        db.session.add(ticket_request)    
        db.session.commit()
       
        print(tags)
        for tag in tags:
            print(ticket.id, tag["name"])
            db.session.add(TicketTag(ticket_id=ticket.id, tag=tag["name"]))            
        db.session.commit()

@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/ticket/subscribe/<string:ticket_id>')
class SubscribeTicket(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_with({}, mask=None)
    def post(self, ticket_id):
        req_data = json.loads(request.data)

        tr = TicketRequester(requester_user_id=req_data["id"], ticket_id=ticket_id)
        db.session.add(tr)    
        db.session.commit()



@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/ticket/response/<string:ticket_id>')
class ResponseTicket(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_with({}, mask=None)
    def post(self, ticket_id):
        req_data = json.loads(request.data)

        t = Ticket.query.filter_by(id=ticket_id).first()
        t.responder_id = req_data["id"]
        t.status = "Chatting"
        print(t.id)
        db.session.commit()



@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/ticket/start/<string:ticket_id>')
class StartTicket(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_with({}, mask=None)
    def post(self, ticket_id):
        t = Ticket.query.filter_by(id=ticket_id).first()
        print(t.status)
        t.status = "In_progress"
        t.started_dttm = datetime.now()
        print(t)
        db.session.commit()

@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/ticket/finish/<string:ticket_id>')
class FinishTicket(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_with({}, mask=None)
    def post(self, ticket_id):
        t = Ticket.query.filter_by(id=ticket_id).first()
        print(t.status)
        t.status = "Waiting_for_review"
        t.finished_dttm = datetime.now()
        t.requested_reward = int((t.finished_dttm - t.started_dttm).total_seconds())
        db.session.commit()

@api.response(403, "Not allowed")
@api.response(404, "Not Found")
@api.response(412, "Precondition Failed")
@api.response(200, "Success")
@api.route('/ticket/review/<string:ticket_id>')
class ReviewTicket(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_with({}, mask=None)
    def post(self, ticket_id):
        user_id = json.loads(request.data)["id"]
        user = User.query.filter_by(id=user_id).first()

        db.session.add(TicketReview(user_id=user_id,
            ticket_id=ticket_id,
            score1=1,
            score2=1,
            score3=1))
        db.session.commit()

        reviews = TicketReview.query \
            .with_entities(
                func.count(TicketReview.user_id).label("number")) \
            .filter_by(ticket_id=ticket_id) \
            .group_by(TicketReview.ticket_id) \
            .first()
        print(reviews)

        requesters = TicketRequester.query \
            .with_entities(
                func.count(TicketRequester.requester_user_id).label("number")) \
            .filter_by(ticket_id=ticket_id) \
            .group_by(TicketRequester.ticket_id) \
            .first()
        print(requesters)

        ticket = Ticket.query.filter_by(id=ticket_id).first()
        reward = int((ticket.finished_dttm - ticket.started_dttm).total_seconds())/requesters.number

        if ticket.responder_id == user.id:
            user.balance += reward
        else:
            user.balance -= reward

        if (reviews.number == requesters.number + 1):            
            ticket.status = 'Closed'

        db.session.commit()

@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/ticket/review/<string:ticket_id>')
class ReviewTicket(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_with({}, mask=None)
    def post(self, ticket_id):
        user_id = json.loads(request.data)["user_id"]

@api.response(412, "Precondition Failed")
@api.response(200, "Success")
@api.route('/auth/register')
# Not implemented
class Register(Resource):
    @api.expect(register_model, validate=True)
    def post(self):
        data = json.loads(request.data)

        email = data["email"]
        password = data["password"]

        user = User.query.filter_by(email=email).first()
        
        if user:
            api.abort(412)

        new_user = User(
            email=email,
            last_name=data["last_name"],
            first_name=data["first_name"],
            balance=100,
            password=generate_password_hash(password, method='sha256'))

        db.session.add(new_user)
        db.session.commit()
        return "user created"

@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/auth/login')
class Login(Resource):
    @api.expect(user_auth_model, validate=True)
    @api.marshal_with(user_response_model, mask=None)
    def post(self):
        data = json.loads(request.data)
        print(data)
        email = data["email"]
        password = data["password"]

        user = User.query.filter_by(email=email).first()
        
        if not user or not check_password_hash(user.password, password):
            api.abort(401)
        
        print("Success!")
        user_data = {
            "id": user.id,
            "email": user.email,
            "last_name": user.last_name,
            "first_name": user.first_name,
            "birth_date": "" if user.birth_date is None else user.birth_date.strftime("%Y-%m-%d %H:%M:%S"),
            "balance": user.balance,
            "avatar": user.avatar
        }
        return user_data


@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/user')
class GetUser(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_with(user_response_model, mask=None)
    def post(self):
        user_id = json.loads(request.data)["id"]
        user = User.query.filter_by(id=user_id).first()

        if not user :
            api.abort(401)
        
        responce = {(k):(v.strftime("%Y-%m-%d %H:%M:%S") if k in ["birth_date"] else v) for k, v in user.__dict__.items() if k not in ["_sa_instance_state", "password"] and v is not None}
        return responce


@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/user/<string:another_user_id>')
class AnotherUser(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_with(user_response_model, mask=None)
    def post(self, another_user_id):
        # TODO: проверка доступа к другому пользователю
        # user_id = json.loads(request.data)["user_id"]

        user = User.query.filter_by(id=another_user_id).first()
        responce = {(k):(v.strftime("%Y-%m-%d %H:%M:%S") if k in ["birth_date"] else v) for k, v in user.__dict__.items() if k not in ["_sa_instance_state", "password"] and v is not None}
        return responce


workspace_model = api.model("Workspace", {
    "id":              fields.String(default=default_field["uuid"], required=True),
    "name":            fields.String(default=default_field["str"], required=True),
    "is_private":      fields.Boolean(default=default_field["bool"], required=True),
    "description":     fields.String(),
    "organisation_id": fields.String(),
    "user_number":     fields.Integer(default=default_field["int"], required=True),
})

create_workspace_model = api.model("CreateWorkspace", {
    "user_id":         fields.String(default=default_field["uuid"], required=True),
    "name":            fields.String(default=default_field["str"], required=True),
    "is_private":      fields.Boolean(default=default_field["bool"], required=True),
    "description":     fields.String(),
    "organisation_id": fields.String(),
})

workspace_user_model = api.model("WorkspaceUser", {
    "user_id": fields.String(default=default_field["uuid"], required=True),
    "role":    fields.String(default=default_field["str"], required=True),
})


@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/workspace')
class GetWorkspaces(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_list_with(workspace_model, mask=None)
    def post(self):
        user_id = json.loads(request.data)["id"]

        workspaces = Workspace \
            .query \
            .join(WorkspaceUser, Workspace.id==WorkspaceUser.workspace_id) \
            .add_columns(
                Workspace.id,
                Workspace.name,
                Workspace.is_private,
                Workspace.description,
                Workspace.organisation_id) \
            .filter(WorkspaceUser.user_id == user_id) \
            .all()

        response = []
        for workspace in workspaces:
            description = "" if workspace.description is None else workspace.description
            organisation_id = "" if workspace.organisation_id is None else workspace.organisation_id

            workspace_users = WorkspaceUser.query \
                .with_entities(
                    func.count(WorkspaceUser.user_id).label("number")) \
                .filter(WorkspaceUser.workspace_id == workspace.id) \
                .group_by(WorkspaceUser.workspace_id) \
                .first()
            print(workspace_users[0])

            response.append(
                {"id": workspace.id,
                "name": workspace.name,
                "private": workspace.is_private,
                "description": description,
                "organisation_id": organisation_id,
                "user_number": workspace_users[0]})
        return response


@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/workspace/users/<string:workspace_id>')
class GetWorkspaceUsers(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_list_with(workspace_user_model, mask=None)
    def post(self, workspace_id):
        # user_id = json.loads(request.data)["user_id"]
        # TODO: добавить проверку полномочий

        ws_users = WorkspaceUser.query.filter_by(workspace_id=workspace_id).all()
        
        response = []
        for user in ws_users:
            print(user.__dict__.items())
            response.append({k:v for k, v in user.__dict__.items() if k != "workspace_id" })
    
        print(response)
        return response


@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/workspace/create')
class CreateWorkspace(Resource):
    @api.expect(create_workspace_model, validate=True)
    @api.marshal_with(workspace_model, mask=None)
    def post(self):
        req_data = json.loads(request.data)
        user_id = req_data.pop("user_id")
        print(req_data)
        ws = Workspace(**req_data)
        db.session.add(ws)            
        db.session.commit()
        db.session.refresh(ws)

        db.session.add(WorkspaceUser(workspace_id=ws.id, user_id=user_id, role="Moderator"))            
        db.session.commit()

message_model = api.model("Message", {
    "id": fields.String(default=default_field["uuid"], requiered=True),
    "ticket_id": fields.String(default=default_field["uuid"], requiered=True),
    "text":    fields.String(default=default_field["str"], requiered=True),
    "created_dttm": fields.String(default=default_field["dttm"], requiered=True),
    "user": fields.Nested(user_response_model)
}, strict=True)


@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/chat/<string:ticket_id>')
class GetChat(Resource):
    @api.expect(user_request_model, validate=True)
    @api.marshal_list_with(message_model, mask=None)
    def post(self, ticket_id):
        ms = Message.query.join(User, User.id==Message.user_id).add_columns(Message.id, Message.ticket_id, Message.text, Message.created_dttm, User.id, User.email, User.last_name, User.first_name, User.birth_date, User.balance, User.password, User.avatar).order_by(Message.created_dttm.desc()).filter(Message.ticket_id == ticket_id).all()

        response = []
        for m in ms:
            print(m)
            res = {
                "id": m[1],
                "ticket_id": m[2],
                "text": m[3],
                "created_dttm": m[4].strftime("%Y-%m-%d %H:%M:%S"),
                "user": {
                    "id": m[5],
                    "email": m[6],
                    "last_name": m[7],
                    "first_name": m[8],
                    "birth_date": "" if m[9] is None else m[9].strftime("%Y-%m-%d %H:%M:%S"),
                    "balance": m[10],
                    "avatar": "" if m[12] is None else m[11].strftime("%Y-%m-%d %H:%M:%S")
                }
                
            }
            response.append(res)
        return response


sent_message_model = api.model("Message", {
    "user_id": fields.String(default=default_field["uuid"], requiered=True),
    "text":    fields.String(default=default_field["str"], requiered=True),
}, strict=True)


@api.response(403, "Not allowed")
@api.response(200, "Success")
@api.route('/chat/send/<string:ticket_id>')
class SendMessage(Resource):
    @api.expect(sent_message_model, validate=True)
    def post(self, ticket_id):
        data = json.loads(request.data)

        data["ticket_id"] = ticket_id
        db.session.add(Message(**data))
        db.session.commit()

print(psycopg2.__version__)

