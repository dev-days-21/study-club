DROP SCHEMA IF EXISTS STUDY_CENTER CASCADE;
CREATE SCHEMA STUDY_CENTER;

DROP TYPE IF EXISTS study_center.ticket_status;
CREATE TYPE study_center.ticket_status AS ENUM ('Open', 'Chatting', 'In_progress', 'Waiting_for_review', 'Expired', 'Closed');
--ALTER TYPE study_center.ticket_status ADD VALUE 'Chatting' AFTER 'Open';

DROP TABLE IF EXISTS study_center.ticket;
CREATE TABLE IF NOT EXISTS study_center.ticket
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(), 
    title TEXT NOT NULL,
    description TEXT NOT NULL,
    created_dttm TIMESTAMP NOT NULL DEFAULT current_timestamp,
    "status" study_center.ticket_status,
    priority DECIMAL,
    workspace_id uuid NOT NULL,
    responder_id uuid,
    responder_message TEXT,
    expires_in DECIMAL,
    last_updated_dttm TIMESTAMP NOT NULL DEFAULT current_timestamp
);

DROP TABLE IF EXISTS study_center."user";
CREATE TABLE IF NOT EXISTS study_center."user"
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    email TEXT UNIQUE NOT NULL,
    last_name TEXT NOT NULL,
    first_name TEXT NOT NULL,
    birth_date date,
    balance INT NOT NULL,
    password TEXT NOT NULL,
    avatar TEXT
);

DROP TABLE IF EXISTS study_center.workspace;
CREATE TABLE IF NOT EXISTS study_center.workspace
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL,
    is_private BOOLEAN NOT NULL DEFAULT False,
    description TEXT,
    organisation_id uuid 
);

DROP TABLE IF EXISTS study_center.ticket_review;
CREATE TABLE IF NOT EXISTS study_center.ticket_review
(	
	id uuid NOT NULL UNIQUE DEFAULT uuid_generate_v4(),
    ticket_id uuid NOT NULL,
    requester_user_id uuid NOT NULL,    
    score1 DECIMAL NOT NULL CHECK (score1 > 0 AND score1 <= 10),
    score2 DECIMAL NOT NULL CHECK (score2 > 0 AND score2 <= 10),
    score3 DECIMAL NOT NULL CHECK (score3 > 0 AND score3 <= 10),
    created_dttm TIMESTAMP NOT NULL DEFAULT current_timestamp,
    PRIMARY KEY (ticket_id, requester_user_id, id)
);

DROP TABLE IF EXISTS study_center.organisation;
CREATE TABLE IF NOT EXISTS study_center.organisation
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL,
    desription TEXT,
    logo TEXT
);

DROP TABLE IF EXISTS study_center.organisation_link;
CREATE TABLE IF NOT EXISTS study_center.organisation_link
(
    url TEXT PRIMARY KEY,
    organisation_id uuid NOT NULL,
    "text" TEXT NOT NULL
);

DROP TYPE IF EXISTS study_center.workspace_user_role;
CREATE TYPE study_center.workspace_user_role AS ENUM ('Teacher', 'Moderator', 'Student');

DROP TABLE IF EXISTS study_center.workspace_user;
CREATE TABLE IF NOT EXISTS study_center.workspace_user
(
    workspace_id uuid NOT NULL,
    user_id uuid NOT NULL,
    role TEXT,
    PRIMARY KEY (workspace_id, user_id)
);



DROP TABLE IF EXISTS study_center.transaction;
CREATE TABLE IF NOT EXISTS study_center.transaction
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    from_user_id uuid,
    to_user_id uuid,
    amount MONEY NOT NULL,
    created_dttm TIMESTAMP NOT NULL DEFAULT current_timestamp
);

DROP TABLE IF EXISTS study_center.ticket_tag;
CREATE TABLE IF NOT EXISTS study_center.ticket_tag
(
    ticket_id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    tag TEXT NOT NULL,
    PRIMARY KEY (ticket_id, tag)
);

DROP TYPE IF EXISTS study_center.reward_request_status;
CREATE TYPE study_center.reward_request_status AS ENUM ('Waiting', 'Approved', 'Executed');

DROP TABLE IF EXISTS study_center.reward_request;
CREATE TABLE IF NOT EXISTS study_center.reward_request
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    responder_user_id uuid NOT NULL,
    ticket_id uuid NOT NULL,
    amount INT NOT NULL,
    "status" study_center.reward_request_status NOT NULL,
    UNIQUE (responder_user_id, ticket_id)
);

DROP TABLE IF EXISTS study_center.ticket_requester;
CREATE TABLE IF NOT EXISTS study_center.ticket_requester
(
    requester_user_id uuid NOT NULL,
    ticket_id uuid NOT NULL,
    created_dttm TIMESTAMP NOT NULL DEFAULT current_timestamp,
    PRIMARY KEY (requester_user_id, ticket_id)
);

DROP TYPE IF EXISTS study_center.workspace_access_request_status;
CREATE TYPE study_center.workspace_access_request_status AS ENUM ('Waiting', 'Approved', 'Rejected');

DROP TABLE IF EXISTS study_center.workspace_access_request;
CREATE TABLE IF NOT EXISTS study_center.workspace_access_request
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL,
    workspace_id uuid NOT NULL,
    "status" study_center.workspace_access_request_status NOT NULL,
    created_dttm TIMESTAMP NOT NULL DEFAULT current_timestamp,
    last_updated_dttm TIMESTAMP NOT NULL DEFAULT current_timestamp
);

DROP TABLE IF EXISTS study_center.message;
CREATE TABLE IF NOT EXISTS study_center.message
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL,
    ticket_id uuid NOT NULL,
    "text" TEXT NOT NULL,
    created_dttm TIMESTAMP NOT NULL DEFAULT current_timestamp
);


ALTER TABLE IF EXISTS study_center.ticket ADD FOREIGN KEY (workspace_id) REFERENCES study_center.workspace (id);
ALTER TABLE IF EXISTS study_center.ticket ADD FOREIGN KEY (responder_id) REFERENCES study_center."user" (id);

ALTER TABLE IF EXISTS study_center.workspace ADD FOREIGN KEY (organisation_id) REFERENCES study_center.organisation (id);

ALTER TABLE IF EXISTS study_center.ticket_review ADD FOREIGN KEY (ticket_id) REFERENCES study_center.ticket (id);
ALTER TABLE IF EXISTS study_center.ticket_review ADD FOREIGN KEY (requester_user_id) REFERENCES study_center."user" (id);

ALTER TABLE IF EXISTS study_center.organisation_link ADD FOREIGN KEY (organisation_id) REFERENCES study_center.organisation (id);

ALTER TABLE IF EXISTS study_center.workspace_user ADD FOREIGN KEY (workspace_id) REFERENCES study_center.workspace (id);
ALTER TABLE IF EXISTS study_center.workspace_user ADD FOREIGN KEY (user_id) REFERENCES study_center."user" (id);

ALTER TABLE IF EXISTS study_center.transaction ADD FOREIGN KEY (from_user_id) REFERENCES study_center."user" (id);
ALTER TABLE IF EXISTS study_center.transaction ADD FOREIGN KEY (to_user_id) REFERENCES study_center."user" (id);

ALTER TABLE IF EXISTS study_center.ticket_tag ADD FOREIGN KEY (ticket_id) REFERENCES study_center.ticket (id);

ALTER TABLE IF EXISTS study_center.reward_request ADD FOREIGN KEY (responder_user_id) REFERENCES study_center."user" (id);
ALTER TABLE IF EXISTS study_center.reward_request ADD FOREIGN KEY (ticket_id) REFERENCES study_center.ticket (id);

ALTER TABLE IF EXISTS study_center.ticket_requester ADD FOREIGN KEY (ticket_id) REFERENCES study_center.ticket (id);
ALTER TABLE IF EXISTS study_center.ticket_requester ADD FOREIGN KEY (requester_user_id) REFERENCES study_center."user" (id);

ALTER TABLE IF EXISTS study_center.workspace_access_request ADD FOREIGN KEY (workspace_id) REFERENCES study_center.workspace (id);
ALTER TABLE IF EXISTS study_center.workspace_access_request ADD FOREIGN KEY (user_id) REFERENCES study_center."user" (id);

ALTER TABLE IF EXISTS study_center.message ADD FOREIGN KEY (ticket_id) REFERENCES study_center.ticket (id);
ALTER TABLE IF EXISTS study_center.message ADD FOREIGN KEY (user_id) REFERENCES study_center."user" (id);
