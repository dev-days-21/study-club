const server = require('http').createServer();
const io = require('socket.io')(server);

let sequenceNumberByClient = new Set();

io.on('connection', client => {
    console.info(`Client connected [id=${client.id}]`);

    sequenceNumberByClient.add(client);

    client.on('UPDATE_CLIENT', data => {
        io.local.emit('UPDATE_SERVER', data);
    });

    client.on("disconnect", () => {
        sequenceNumberByClient.delete(client);
        console.info(`Client gone [id=${client.id}]`);
    });
});


server.listen(3001);